# Minimega Linux Bridge Test

This test case provides coverage for the Minimeaga Linux bridge functionality.

## Running

First install [Raven](https://gitlab.com/mergetb/tech/raven).

```
sudo su
./run.sh
```

Then ssh into each of the nodes and run Minimega

```
ssh <node>
sudo /tmp/mm/tests/raven/linux-bridge/launch-minimega.sh
```

### Adding VTEPS

```
ip link add vxlan100 type vxlan id 100 dstport 4789 local $localip dev $ifx
bridge fdb append 00:00:00:00:00:00 dev vxlan100 dst $dst
ip link set up dev vxlan100
ip link set master mega_bridge dev vxlan100
bridge vlan add vid 100 dev vxlan100
```

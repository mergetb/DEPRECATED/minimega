# common
namespace test
bridge linux
vm config disk /tmp/debian-buster.qcow2
vm config net 100
vm config cpu qemu64

vm config schedule a
vm launch kvm v0
vm start v0

vm config schedule b
vm launch kvm v1
vm start v1

vm config schedule c
vm launch kvm v2
vm start v2

topo = {
    name: 'minimega_linuxbridge',
    nodes: [deb('rex'), deb('a'), deb('b'), deb('c')],
    switches: [cumulus('sw')],
    links: [
        Link('a', 1, 'sw', 1),
        Link('b', 1, 'sw', 2),
        Link('c', 1, 'sw', 3),
        Link('rex', 1, 'sw', 4),
    ]
}

function deb(name) {
  return {
    name: name,
    image: 'debian-bullseye',
    cpu: { cores: 8 },
    memory: { capacity: GB(8) },
    mounts: [{ source: env.PWD+'/../../..', point: '/tmp/mm' }],
    vclock: true,
  }
}

function cumulus(name) {
  return {
    name: name,
    image: 'cumulusvx-4.2',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
  }
}

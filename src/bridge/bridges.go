// Copyright (2016) Sandia Corporation.
// Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
// the U.S. Government retains certain rights in this software.

package bridge

import (
	"errors"
	"fmt"
	log "minilog"
	"os"
	"path/filepath"
	"time"

	"github.com/google/gopacket/pcap"
	"gitlab.com/mergetb/tech/rtnl"
)

type BridgeType int

const (
	OVS = iota
	LINUX
)

// Bridges manages a collection of `Bridge` structs.
type Bridges struct {
	Default string // Default bridge name when one isn't specified

	Type BridgeType

	nameChan chan string
	bridges  map[string]BridgeDev
}

// openflow filters to redirect arp and icmp6 traffic to the local tap
var snoopFilters = []string{
	"dl_type=0x0806,actions=local,normal",
	"dl_type=0x86dd,nw_proto=58,icmp_type=135,actions=local,normal",
}

// snoopBPF filters for ARP and Neighbor Solicitation (NDP)
const snoopBPF = "(arp or (icmp6 and ip6[40] == 135))"

// NewBridges creates a new Bridges using d as the default bridge name and f as
// the format string for the tap names (e.g. "mega_tap%v").
func NewBridges(d, f string) *Bridges {
	nameChan := make(chan string)

	// Start a goroutine to generate tap names for us
	go func() {
		defer close(nameChan)

		for tapCount := 0; ; tapCount++ {
			tapName := fmt.Sprintf(f, tapCount)
			fpath := filepath.Join("/sys/class/net", tapName)

			if _, err := os.Stat(fpath); os.IsNotExist(err) {
				nameChan <- tapName
			} else if err != nil {
				log.Fatal("unable to stat file -- %v %v", fpath, err)
			}

			log.Debug("tapCount: %v", tapCount)
		}
	}()

	b := &Bridges{
		Default:  d,
		nameChan: nameChan,
		bridges:  map[string]BridgeDev{},
	}

	// Start a goroutine to collect bandwidth stats every 5 seconds
	go func() {
		for {
			time.Sleep(5 * time.Second)

			b.updateBandwidthStats()
		}
	}()

	return b
}

func (b Bridges) newBridge(name string) error {

	switch b.Type {
	case OVS:
		return b.newOvsBridge(name)
	case LINUX:
		return b.newLinuxBridge(name)
	default:
		return fmt.Errorf("unknown bridge type: %v", b.Type)
	}

}

// newBridge creates a new bridge with ovs, assumes that bridgeLock is held.
func (b Bridges) newOvsBridge(name string) error {
	log.Info("creating new bridge: %v", name)

	br := &Bridge{
		Name:     name,
		taps:     make(map[string]*Tap),
		trunks:   make(map[string]bool),
		tunnels:  make(map[string]bool),
		mirrors:  make(map[string]bool),
		captures: make(map[int]capture),
		nameChan: b.nameChan,
		config:   make(map[string]string),
	}

	// Create the bridge
	created, err := ovsAddBridge(br.Name)
	if err != nil {
		return err
	}

	br.preExist = !created

	// Bring the interface up, start MAC <-> IP learner
	if err = upInterface(br.Name, false); err != nil {
		goto cleanup
	}

	for _, filter := range snoopFilters {
		if err = br.addOpenflow(filter); err != nil {
			goto cleanup
		}
	}

	if br.handle, err = pcap.OpenLive(br.Name, 1600, true, time.Second); err != nil {
		goto cleanup
	}

	if err = br.handle.SetBPFFilter(snoopBPF); err != nil {
		goto cleanup
	}

	go br.snooper()

	// No errors... bridge ready for use
	b.bridges[br.Name] = br
	return nil

cleanup:
	if err != nil {
		log.Errorln(err)
	}

	if br.handle != nil {
		br.handle.Close()
	}

	// Try to delete the bridge, if we created it
	if created {
		if err := ovsDelBridge(br.Name); err != nil {
			// Welp, we're boned
			log.Error("zombie bridge -- %v %v", br.Name, err)
		}
	}

	return err
}

func (b Bridges) newLinuxBridge(name string) error {
	log.Info("creating new linux bridge: %v", name)

	lb := &LinuxBridge{
		Name: name,
		taps: make(map[string]*Tap),
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}

	br := rtnl.NewLink()
	br.Info.Name = name
	br.Info.Bridge = &rtnl.Bridge{VlanAware: true}

	err = br.Present(ctx)
	if err != nil {
		return err
	}

	err = br.Up(ctx)
	if err != nil {
		return err
	}

	b.bridges[name] = lb
	return nil

}

// Names returns a list of all the managed bridge names.
func (b Bridges) Names() []string {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	res := []string{}
	for k := range b.bridges {
		res = append(res, k)
	}

	return res
}

// Get a bridge by name. If one doesn't exist, it will be created.
func (b Bridges) Get(name string) (BridgeDev, error) {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	if name == "" {
		name = b.Default
	}

	// Test if the bridge already exists
	if v, ok := b.bridges[name]; ok {
		return v, nil
	}

	// Doesn't exist, create it
	if err := b.newBridge(name); err != nil {
		return nil, err
	}

	return b.bridges[name], nil
}

// HostTaps returns a list of taps that are marked as host taps.
func (b Bridges) HostTaps() []Tap {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	res := []Tap{}

	for _, br := range b.bridges {
		res = append(res, br.GetTaps()...)
	}

	return res
}

// Info collects `BridgeInfo` for all managed bridges.
func (b Bridges) Info() []BridgeInfo {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	res := []BridgeInfo{}

	for _, br := range b.bridges {

		res = append(res, br.BridgeInfo())
	}

	return res
}

// Destroy calls `Bridge.Destroy` on each bridge, returning the first error.
func (b Bridges) Destroy() error {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	for k, br := range b.bridges {
		if err := br.Destroy(); err != nil {
			return err
		}

		delete(b.bridges, k)
	}

	return nil
}

// DestroyBridge destroys a bridge by name, removing all of the taps, etc.
// associated with it.
func (b Bridges) DestroyBridge(name string) error {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	br, ok := b.bridges[name]
	if !ok {
		return fmt.Errorf("bridge not found: %v", name)
	}

	if err := br.Destroy(); err != nil {
		return err
	}

	delete(b.bridges, name)
	return nil
}

// ReapTaps calls `Bridge.ReapTaps` on each bridge, returning the first error.
func (b Bridges) ReapTaps() error {

	// XXX: done on already by ReapTaps, if this needs to be done here, the lock
	// in the ReapTaps function would need to be optional to prevent deadlock
	//bridgeLock.Lock()
	//defer bridgeLock.Unlock()

	for _, br := range b.bridges {
		if err := br.ReapTaps(); err != nil {
			return err
		}
	}

	return nil
}

// FindTap finds a non-defunct Tap with the specified name. This is
// non-deterministic if there are multiple taps with the same name.
func (b Bridges) FindTap(t string) (Tap, error) {
	bridgeLock.Lock()
	defer bridgeLock.Unlock()

	log.Debug("searching for tap %v", t)

	for _, br := range b.bridges {
		tap := br.FindTap(t)
		if tap != nil {
			return *tap, nil
		}
	}

	return Tap{}, errors.New("unknown tap")
}

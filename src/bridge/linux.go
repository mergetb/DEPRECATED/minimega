package bridge

import (
	"fmt"
	"gonetflow"
	log "minilog"
	"strings"
	"sync/atomic"

	"gitlab.com/mergetb/tech/rtnl"
)

type LinuxBridge struct {
	Name string

	taps        map[string]*Tap
	isdestroyed uint64
}

func (b *LinuxBridge) Config(s string) error {
	// at this time linux bridge does not take in any extra configuration
	return nil
}

func (b *LinuxBridge) CreateTap(mac string, vlan int) (string, error) {

	name := fmt.Sprintf("tap%d", len(b.taps))
	log.Info("creating linux tap: %v", name)

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return "", err
	}

	bridge, err := rtnl.GetLink(ctx, b.Name)
	if err != nil {
		return "", err
	}

	err = createTap(name)
	if err != nil {
		log.Info("creating tap failed")
		return "", err
	}

	lnk, err := rtnl.GetLink(ctx, name)
	if err != nil {
		log.Info("getting tap failed")
		return "", err
	}

	err = lnk.Set(ctx)
	if err != nil {
		log.Info("failed to set tap mac")
		return "", err
	}

	err = lnk.SetMaster(ctx, int(bridge.Msg.Index))
	if err != nil {
		log.Info("set tap bridge failed")
		return "", err
	}

	err = lnk.Up(ctx)
	if err != nil {
		log.Info("set tap up failed")
		return "", err
	}

	err = lnk.SetUntagged(ctx, uint16(vlan), false, true, false)
	if err != nil {
		log.Info("set tap untagged failed")
		return "", err
	}

	b.taps[name] = &Tap{
		Name:   name,
		Bridge: b.Name,
		VLAN:   vlan,
		MAC:    mac,
	}

	return name, nil

}

func (b *LinuxBridge) DestroyTap(t string) error {

	tap, ok := b.taps[t]
	if !ok {
		return fmt.Errorf("unknown tap: %v", t)
	}

	tap.Defunct = true

	return nil

}

func (b *LinuxBridge) GetTaps() []Tap {

	taps := make([]Tap, 0, len(b.taps))
	for _, t := range b.taps {
		taps = append(taps, *t)
	}
	return taps
}

func (b *LinuxBridge) ReapTaps() error {

	for _, t := range b.taps {
		if t.Defunct {

			ctx, err := rtnl.OpenDefaultContext()
			if err != nil {
				return err
			}

			lnk, err := rtnl.GetLink(ctx, t.Name)
			if err != nil {

				// TODO rtnl should expose this as a typed error
				if strings.Contains(err.Error(), "not found") {
					// nothing to do
					return nil
				}

				return err
			}

			err = lnk.Absent(ctx)
			if err != nil {
				return err
			}

		}
	}

	return nil

}

func (b *LinuxBridge) Destroy() error {

	if b.destroyed() {
		return nil
	}

	b.setDestroyed()

	for _, tap := range b.taps {
		if tap.Defunct {
			continue
		}

		if err := b.DestroyTap(tap.Name); err != nil {
			log.Info("could not destroy tap: %v", err)
		}
	}

	if err := b.ReapTaps(); err != nil {
		return err
	}

	return linuxDelBridge(b.Name)

}

func linuxDelBridge(name string) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}

	lnk, err := rtnl.GetLink(ctx, name)
	if err != nil {
		return err
	}

	err = lnk.Absent(ctx)
	if err != nil {
		return err
	}

	return nil

}

func (b *LinuxBridge) BridgeInfo() BridgeInfo {

	info := BridgeInfo{
		Name: b.Name,
	}

	for _, t := range b.taps {
		info.VLANs = append(info.VLANs, t.VLAN)
	}

	return info

}

func (b *LinuxBridge) AddTap(tap, mac string, lan int, host bool) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) RemoveTap(tap string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) UpdateQos(tap string, op QosOption) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) RemoveQos(tap string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) CreateHostTap(tap string, lan int) (string, error) {
	return "", fmt.Errorf("not implemented")
}
func (b *LinuxBridge) CreateContainerTap(tap, ns, mac string, vlan, index int) (string, error) {
	return "", fmt.Errorf("not implemented")
}
func (b *LinuxBridge) GetQos(tap string) []QosOption {
	return nil
}
func (b *LinuxBridge) FindTap(string) *Tap {
	return nil
}
func (b *LinuxBridge) CreateMirror(src, dst string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) DestroyMirror(tap string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) AddTrunk(iface string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) RemoveTrunk(iface string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) AddTunnel(typ TunnelType, remoteIP, key string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) RemoveTunnel(iface string) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) Capture(fname string, config ...CaptureConfig) (int, error) {
	return -1, fmt.Errorf("not implemented")
}
func (b *LinuxBridge) StopCapture(id int) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) CaptureTap(tap, fname string, config ...CaptureConfig) (int, error) {
	return -1, fmt.Errorf("not implemented")
}
func (b *LinuxBridge) GetNetflow() (*gonetflow.Netflow, error) {
	return nil, fmt.Errorf("not implemented")
}
func (b *LinuxBridge) NewNetflow(timeout int) (*gonetflow.Netflow, error) {
	return nil, fmt.Errorf("not implemented")
}
func (b *LinuxBridge) DestroyNetflow() error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) SetNetflowTimeout(timeout int) error {
	return fmt.Errorf("not implemented")
}
func (b *LinuxBridge) UpdateBandwidthStats() {
}
func (b *LinuxBridge) BandwidthStats() (float64, float64) {
	return 0.0, 0.0
}

// Helpers --------------------------------------------------------------------

func (b *LinuxBridge) setDestroyed() {
	atomic.StoreUint64(&b.isdestroyed, 1)
}

func (b *LinuxBridge) destroyed() bool {
	return atomic.LoadUint64(&b.isdestroyed) > 0
}
